-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 01 juin 2020 à 16:39
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Asterix', 'fgetzt'),
(2, 'Gaston Lagaffe', 'trhry'),
(3, 'Lucky Luke', 'dfe'),
(4, 'Spirou', ''),
(5, 'Tintin', ''),
(6, 'Boule & Bill', '');

-- --------------------------------------------------------

--
-- Structure de la table `filter`
--

DROP TABLE IF EXISTS `filter`;
CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `filter`
--

INSERT INTO `filter` (`id`, `name`) VALUES
(1, 'objet'),
(2, 'vaisselle');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `products_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_image_id` (`products_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `name`, `products_id`) VALUES
(1, 'Spirou/Fanta1', 71);

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `panier_product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `description`, `short_description`, `price`, `category_id`) VALUES
(1, 'Abraracourcix', 'Asterix/Abraracourcix.jpg ', '             <p>Get the lastest and trending web development project\'s source code, first see preview if you need then get the code.\r\n', 'ny mugg', 245, 1),
(3, 'Asterix & Obelix', 'Asterix/Asterix&Obelix.jpg', 'Voici une statuette de Astérix et Obélix, issue de l\'univers d\'Asterix, un personnage supplémentaire pour compléter la galerie issue de l\'univers du plus fameux Gaulois ! Vous en avez rêvé et Leblon-Delienne l\'a fait !', 'Cereal reese travis scott', 339, 1),
(4, 'Asterix', 'Asterix/asterix.png', 'Sur cette figurine, Astérix lit l\'une de ses meilleures aventures : \"Astérix chez les Bretons\".Figurine CollectoysAnnée : 2018Matière : résineDimensions : 21 x 14 x 23,5 cm', 'azaear', 435, 1),
(5, 'Bonemine', 'Asterix/Bonemine.jpg', 'Retrouvez Bonemine en figurine sur CollectorBD !Elle est en train de coudre, assise sur un tabouret. Le sourcil levé, elle écoute attentivement ce qui se passe autour d\'elle', 'zeaztr', 220, 1),
(6, 'Legionnaire', 'Asterix/Legionnaire.jpg', 'Une superbe figurine d\'une vingtaine de centimètres de haut, en résine. Cette magnifique statuette représente les deux héros tels qu\'ils sont dessinés sur la couverture du mythique album : Astérix légionnaire.', 'gfshf', 320, 1),
(7, 'Obelix', 'Asterix/obelix.jpg', 'Comme il est tombé dans le chaudron de potion magique quand il était petit, cette pile d\'albums des aventures des Gaulois n\'est pas plus lourde qu\'un menhir !', '', 123, 1),
(8, 'Lilo', 'Asterix/vieuxAsteric.jpg', 'D\'après l\'univers d\'Astérix et Obélix par Uderzo et Goscinny.  Hauteur : 12 cm.  Tirage limité à 350 exemplaires.', '', 226, 1),
(9, 'Agent Longtarin', 'GastonLagaffe/AgentLongtarin.jpg', 'Son air aussi hagard que goguenard nous laisse présager le pire : un PV est sûrement prêt a être servi ! Mais la panoplie de L\'Agent Longtarin ne saurait être complète sans son parcmètre.', '', 330, 2),
(10, 'Mlle Jeanne', 'GastonLagaffe/Cactus.jpg', 'Nous connaissions Gaston pour ses nombreux déguisements et notamment celui du cactus mais vous, Mlle Jeanne, c\'est une première !', 'Sculpteur : Pascal Rodier <br>\r\nHauteur : 20 cm <br>\r\nMatière : résine <br>\r\n Année d\'édition : 2007', 145, 2),
(11, 'Prunelle', 'GastonLagaffe/Prunellee.jpg', 'Qui d\'autre que Gaston pourrait être sommé de rappliquer par Prunelle de cette façon ?! Qui veut parier que Gaston a encore eu une idée géniale ?', 'Sculpteur : Pascal Rodier Hauteur : 21 cm Matière : Résine Année d\'édition : 2005 Tirage : 750 exemplaires', 131, 2),
(13, 'Gaston & Marsupilami', 'GastonLagaffe/Gaston&Marsupilami.jpg', ' Gaston, aurais-tu confondu ton lit avec celui des bébés Marsupilami ? Cela nous donnerait presque envie d\'aller roupiller dans ce joli nid de plumes, très douillet, surtout en présence des bébés Marsupilami !', ' Figurine en résine - scène prestige Figurine montée sur un socle ovale noir. Taille : 31cm.*', 139, 2),
(14, 'Panoramix', 'Asterix/Panoramix.jpg', 'Retrouvez le grand Panoramix, druide vénérable du Village, détenteur du savoir, et notamment du secret de la potion magique dont il a lui-même créé la recette !', '', 234, 1),
(15, 'Agecanonix', 'Asterix/Agecanonix.png', 'D\'après l\'univers d\'Asterix d\'Uderzo et Goscinny.  Sculpteur : Pascal Rodier Auteur(s) : Uderzo et Goscinny Hauteur : 18 cm Matière : Résine  Tirage : 350 ex. accompagnés d\'un certificat.', '', 136, 1),
(16, 'Gaston & Jeanne', 'GastonLagaffe/gason&Jeanne.jpg', 'Nous connaissons sa dextérité - parfois approximative - au volant de sa FIAT mais qu\'en est-il d\'un deux roues motorisé ? Mlle Jeanne, en tout cas, est prête à le découvrir. ', 'Collection Le Garage de Franquin par Figures & vous Matière : Résine et métal Longueur : 17 cm', 170, 2),
(17, 'Gaston', 'GastonLagaffe/gaston.jpg', 'La posture, la gaieté et la malice de Gaston ont immédiatement séduits le sculpteur de chez BC Arts design qui a choisi de le représenter et de l\'interpréter en compagnie de son célèbre chat.', 'Format : 36 cm + socle. <br>\r\nPoids : 6 kg <br>\r\nMatière : résine composite. \r\n', 130, 2),
(18, 'Gaston Car', 'GastonLagaffe/GastonCar.jpg', 'Cette figurine a été conçue en exclusivité par Raiarox. Le concept étant de mélanger une figurine à la façon Fariboles dans un véhicule à la mode, l\'auto tamponneuse, par CG Models.', 'Hauteur : 20 cm <br> Matière : Métal et Résine <br> Exclusivité Raiarox <br>Fabrication : Fariboles / CG <br>Model  Tiré à 200 exemplaires et livré dans un coffret en bois sérigraphié', 169, 2),
(19, 'Gaston et son chat', 'GastonLagaffe/gastonChat.jpg', ' Le moins qu\'on puisse dire c\'est que le style de Gaston est intemporel et confortable ! Ce n\'est pas son chat qui dira le contraire, bien installé dans le capuche du gaffeur !', 'D\'après l\'univers de Gaston Lagaffe par Franquin. <br> Hauteur : 18cm <br>Matière : résine', 214, 2),
(20, 'Caisse Fragile', 'GastonLagaffe/GastonCaisseFragile.jpg', 'Qu\'est ce qui a donc ému aux larmes notre cher Gaston ? Un rendez-vous annulé par Mademoiselle Jeanne ou une énième réprimande de l\'agent Longtarin ?', 'Année d\'édition : 2018 <br> Hauteur: 12 cm <br> Matière: résine Peinte à la main', 346, 2),
(21, 'Prunelle', 'GastonLagaffe/Prunelle.jpg', 'Qui aurait pu l\'imaginer se prélasser dans un fauteuil avec un verre à la main ? Rien à voir avec le rédacteur en chef stressé et torturé qui arpente les bureaux du Journal de Spirou.', 'Sculpteur: Pascal Rodier <br> Hauteur : 11 cm <br> Matière : Résine Peinte à la main <br> Exclusivité Raiarox Passion', 177, 2),
(22, 'Gaston & le Taxi', 'GastonLagaffe/TaxiGaston.jpg', 'La figurine représente Gaston, devant son Taxi qui tend ses clés. Il est garé devant un panneau interdit de stationner (ce qui, quand on connaît Gaston, n\'est pas très étonnant).', '', 325, 2),
(23, 'Les Dalton à cheval', 'LuckyLuke/DaltonCheval.jpg', 'Cette statuette en résine inspirée d\'une illustration de 1963 pour un album de coloriage paru aux Editions Dupuis a été créée par Fariboles Productions en partenariat avec La Marque Zone (LMZ Collection).', 'Dimensions de la figurine : 37 x 17 x 30cm Dimensions de la boîte : 34 x 48 x 16cm Matière : résine', 170, 3),
(24, 'Jane', 'LuckyLuke/Jane.jpg', 'Même si elle n\'apparaît que très peu dans la série, Calamity Jane a marqué les esprits. Redécouvrez ce grand personnage des aventures de Lucky Luke avec cette superbe figurine !', 'Année : 2019 <br>Fabricant : Fariboles <br>Editeur : Zédibulle Hauteur : 25 cm', 179, 3),
(25, 'Joe Dalton', 'LuckyLuke/JoeDalton.jpg', 'Joe Dalton est représenté ici comme s\'échappant d\'une pile d\'albums, un revolver à la main ! Quel mauvais coup a-t-il encore préparé ?', 'Hauteur : 5 cm\r\nLargeur : 5,5 cm\r\nFigurine en plomb et peinte à la main', 435, 3),
(26, 'Lucky Luke', 'LuckyLuke/lucky.jpg', 'Un tonneau, la rue déserte d\'une ville fantôme du Far West, un soleil de plomb et très certainement quelques tireurs embusqués çà et là bien décidés à avoir la peau de \"L\'homme qui tire deux fois plus vite que son ombre\"', 'Un tonneau, la rue déserte d\'une ville fantôme du Far West, un soleil de plomb et très certainement quelques tireurs embusqués çà et là bien décidés à avoir la peau de \"L\'homme qui tire deux fois plus vite que son ombre\"', 172, 3),
(27, 'Luky Luke ', 'LuckyLuke/LuckyCharette.jpg', 'Format : 42 x 17 cm\r\nMatière : Métal (socle en bois)\r\nFigurine Pixi peinte à la main\r\n\r\nTirage limité à 130 exemplaires numérotés accompagnés de leur certificat d\'authenticité', 'onePiece shanks & Luffy', 324, 3),
(28, 'Lucky Luke', 'LuckyLuke/LuckyLuke.jpg', 'Lucky Luke bondit sur Jolly Jumper, qui l\'attendait patiemment près du point d\'attache, et s\'apprête à se mettre en route vers de nouvelles aventures.', '', 17, 3),
(29, 'Lucky Luke', 'LuckyLuke/Luke.jpg', 'Statuette en résine inspirée de l\'aventure de Lucky Luke \"Sur la piste des Dalton\" (1960).Inspirée de l\'oeuvre de Morris & Goscinny, cette statuette a été créée par Fariboles Productions.', '', 17, 3),
(30, 'Ma Dalton', 'LuckyLuke/MaDalton.jpg', 'Oui c\'est bien elle ! La redoutable génitrice des non moins redoutables Daltons ! Elle leur a généreusement légué les pires aspects de sa propre personne : la méchanceté, la convoitise, la sournoiserie ! ', '', 17, 3),
(31, 'Mathias Bones le croque mort', 'LuckyLuke/MathiasBones.jpg', 'Vêtu de son solennel costume noir et tenant son ruban mètre à la verticale, les yeux fermés et esquissant un léger sourire, il se délecte déjà de la bonne odeur de la mort, du costume en sapin et de la terre recouvrant un cercueil tout juste refermé.', '', 17, 3),
(32, 'Rantanplan', 'LuckyLuke/Rantanplan.jpg', 'Plus bête que Rantanplan, plus méchant que Joe Dalton, est-ce possible ? Evidemment non : nous tenons-là deux champions ! On ne sait pas lequel, du coup de masse perfide ou du coup de langue servile, va partir en premier... ', '', 17, 3),
(33, 'Gaston Lagaffe', 'Spirou/Spirou.jpg', '', '', 17, 4),
(34, 'Cam & Leon', 'Spirou/cameleon.jpg', 'Le Marsupilami, ce cher petit animal, a disparu... Que ne feraient Spirou, Fantasio et Spip pour le retrouver, puis l\'extirper des griffes de l\'affreux Zabaglione, directeur de cirque cupide et voleur ?', '', 17, 4),
(35, 'Spirou le groom vert', 'Spirou/SpirouVert.jpg', 'Le \"bel\" uniforme fourni par les boches ? Il en a déjà fait une loque ! Godfermillard de nom de djue ! Il ne sera pas dit que le petit groom va se laisser piétiner par la botte allemande! Tudju ! Un sacré castard, ce Spirou !', '', 17, 4),
(36, 'Spirou hommage Lucky Luke', 'Spirou/SpirouLuckyLuck.jpg', 'Décidément Spirou est un personnage qui a la classe ! Tout lui va ! Habituellement tiré à quatre épingles, il s\'est déguisé en cow-boy pour une occasion toute particulière : fêter les 70 ans (oui , septante !!) de son pote Lucky Luke', '', 17, 4),
(37, 'Vertignasse', 'Spirou/Spirou&Fantasio.jpg', 'Mais de qui se moque-t-il ? Entre le Petit Spirou et son ami Vertignasse, c\'est “à la vie à la mort”, mais cela n\'empêche pas de garder un certain sens critique.', '', 17, 4),
(38, 'Spirou', 'Spirou/Zorglub.jpg', 'Génie mégalomane, méchant au bon coeur, inventeur surdoué tant dans le domaine des sciences que du langage, Zorglub s\'attire immanquablement les faveurs des lecteurs par ses échecs répétés dans sa conquête du monde', '', 17, 4),
(39, 'Les Dupondt', 'Tintin/dupondt.jpg', 'Quelle arrivée fracassante dans les aventures de Tintin ! Voyons Les Dupondt ce n\'est pas de cette manière que vous le retrouverez ce cher Tintin !', '', 17, 5),
(40, 'Bugs Bunny', 'Tintin/Haddock.jpg', 'Redécouvrez le Capitaine Haddock avec cette figurine de collection Moulinsart. On retrouve l\'uniforme du Capitaine : ce fameux col-roulé bleu, sa veste noire et sa casquette de marin qui ne le quittent jamais', '', 17, 5),
(41, 'Nestor au plumeau', 'Tintin/Nestor.jpg', 'Sur cette figurine, Nestor porte sa tenue rayée jaune et noire, tenue qu\'il a dès sa première apparition.', '', 17, 5),
(42, 'Tintin Cow Boy', 'Tintin/Tintin.jpg', 'Paré pour la grande aventure de l\'Ouest, Tintin tirera-t-il plus vite que son ombre ?<br>Matière : résine.<br>Format : 6 x 7 x 11,5 cm.', '', 17, 5),
(43, 'Tintin et Milou', 'Tintin/tintinAstronaute.jpg', 'Retrouvez Tintin et Milou dans leurs combinaisons spatiales grâce à cette superbe figurine. Fabriquée de manière artisanale, en France, par Fariboles, cette figurine représente les deux héros dans le respect des dessins d\'Hergé.', '', 17, 5),
(44, 'Tintin en route !', 'Tintin/tintinEnRoute.jpg', 'Contrairement à son créateur, Tintin a voyagé partout dans le monde. Il a visité de nombreux pays tels que le Congo, la Chine, l\'Egypte, les Etats-Unis ou encore le Tibet et il est même allé jusque sur la Lune !', '', 17, 5),
(45, 'Tintin et Milou', 'Tintin/TintinMilou.jpg', 'Comme à son habitude, notre reporter est toujours prêt pour de nouvelles péripéties. Il revient tout juste de Chine pour repartir en Amazonie ! Mais nous ne vous en dirons pas plus...', '', 17, 5),
(46, 'Tintin et Milou dans la potiche', 'Tintin/tintinVase.jpg', 'Figurine de collection en résine Moulinsart: Tintin et Milou dans la potiche du Lotus bleu, collection \"Les icônes\". Livré en boîte, accompagné d’un certificat d’authenticité numéroté. Hauteur: 20cm.', '', 17, 5),
(47, 'SuperSaiyan', 'Tintin/Tournesol&Haddock.jpg', 'La figurine est en résine. Elle fait partie de la collection « Lune », de même que Tintin et Milou cosmonautes.', '', 17, 5),
(48, 'Tournesol', 'Tintin/Tournesol.jpg', 'Tournesol déambule en étudiant son pendule. Le guidera-t-il à travers l\'exposition du musée imaginaire des aventures de Tintin ?', '', 165, 5),
(49, 'Bill & Pouf', 'Boule&Bill/Bill&Pouf.jpg', 'Serait-ce une course des indiens contre les cow-boys ou Bill en voudrait-il à Pouf ?  Tirage limité à 40 exemplaires.', 'Matière : résine. <br>Format : 11,5 x 8 x 19 cm. <br>Caractéristiques : Modélisation 3D réalisée par David Arnould. Moulée, assemblée et peinte à la main.', 165, 6),
(50, 'Boule et Bill', 'Boule&Bill/Boule&Bil.jpg', 'Les meilleurs amis en pleine action !D\'après l\'univers de Boule & Bill par Roba  Leblon-Delienne  EAN : 3700677944559  H: 23 cm  1 500 exemplaires, en résine.', '', 165, 6),
(51, 'Caroline', 'Boule&Bill/Caroline.jpg', 'Mais qu\'ont encore fait Boule et Bill pour provoquer une telle hilarité ! Caroline la tortue de la famille, douce, rigolote, insomniaque et fan de bobsleigh, est follement amoureuse de Bill…', '', 165, 6),
(52, 'Boule & Bill à la neige', 'Boule&Bill/NeigeBoule.png', 'Cette statuette est une exclusivité La Marque Zone. Elle a été sculptée par Quentin Soubrouillard et réalisée par l\'atelier Phoenix Effect, en France.  Sortez moufles et bonnets et rejoignez Boule et Bill !', '', 150, 6),
(53, 'Boule & Bill à la pêche', 'Boule&Bill/boulePeche.jpg', '', '', 150, 6),
(54, 'Boule & Bill en voiture !', 'Boule&Bill/VehiculeBoule.jpg', 'Caisse à savon ?Caisse à roulettes ? Planches et roues de récup, clous, volant emprunté, freins douteux quand existants,  peinture à l’arrache. Quel gamin ne s’est pas bricolé le bolide de rêve, essayé en pleine rue à l’insu de l’autorité familiale ?', '', 150, 6),
(55, 'Boule & Bill en baignade', 'Boule&Bill/baignade.jpg', 'Une scène cocasse tirée de \"L\'album de famille de Boule et Bill\"  Figurine en résineHauteur : 24cm', '', 150, 6),
(56, 'Boule & Bill punis', 'Boule&Bill/boulepunis.jpg', '', '', 150, 6),
(57, 'Tintin le sous-marin requin', 'Tintin/requin.jpg\r\n', 'Après \"La potiche\", c\'est au tour du \"Sous-marin requin\" de venir compléter la collection \"Les icônes\" de Moulinsart. Chaque pièce fonctionne bien entendu indépendamment.', '', 147, 5),
(58, 'Tintin et Milou a la maison', 'Tintin/Maison.jpg\r\n', 'Cette scène reproduit la case 14 de la planche 2 de L\'oreille cassée, 6ème album des Aventures de Tintin. \"Curieuse coïncidence, ne trouves-tu pas, Milou ?... Il s\'en moque : il dort. Eh bien ! Je vais l\'imiter\", déclare le journaliste.', '', 129, 5),
(59, 'Jules Cesar', 'Asterix/julescesar.jpg\r\n', 'Redécouvrez le personnage de Jules César grâce à cette sublime figurine de chez Fariboles. Grâce à sa posture, on reconnait son air supérieur et l\'orgueil d\'un homme de pouvoir.', '', 1470, 1),
(60, 'Olibrius', 'Asterix/Olibrius.jpg\r\n', 'Olibrius, célèbre légionnaire romain, qui, de retour de sa première patrouille comprendra vite ce qu\'est la vie de légionnaire. A grands coups de gifles, son mot d\'ordre : fuir les Gaulois ', '', 1470, 1),
(61, 'Assurancetourix', 'Asterix/Assurancetourix.jpg', 'Assurancetourix est une figure importante, surtout en tant qu\'éternel souffre-douleur des habitants d\'un village !', '', 1470, 1),
(63, 'Père de Boule énervée', 'Boule&Bill/PereEnerve.jpg', '', '', 1470, 6),
(64, 'Le roi et les Schtroumpf', 'Boule&Bill/livre.jpg', '', '', 1470, 6),
(65, 'Lucky Luke', 'LuckyLuke/LuckyOmbre.jpg', 'Lucky Luke tire plus vite que son ombre\"', '', 1470, 3),
(66, 'Ming Li foo', 'LuckyLuke/minglifoo.jpg', 'La sculpture a été réalisée par Alban FICAT. Elle représente le savoureux personnage Ming Li Foo.', '', 1470, 3),
(67, 'Bill', 'Boule&Bill/bill.jpg', '', '', 1470, 6),
(68, 'Bill', 'Boule&Bill/boule&co.jpg', '', '', 1470, 6),
(69, 'Maire', 'Spirou/Maire.jpg', 'Chut, ne faites plus un bruit, vous risquerez d\'interrompre le maire de Champignac en Cambrousse en plein discours !*', '', 17, 4),
(70, 'L\'enlèvement', 'Spirou/ZorglubGang', '« L\'Enlèvement », est le second triptyque issu des aventures de Spirou & Fantasio par Jean-Claude Fournier. Cette somptueuse saynète est la fidèle reproduction en 3D d\'une case de la planche 11A de l\'album « Le Faiseur d\'or »', '', 1470, 4),
(71, 'Fantasio Hommage Lucky Luke', 'Spirou/FantasioLucky.png', 'Un petit mot de Raiarox:\r\n\"Cette pièce a été sculptée en Corse ! Pascal l\'a crée pendant ses congés ...le matin de bonne heure\r\n, et je sais pas pour vous ...mais moi je le ressens dans la pièce ! Du très grand Pascal !!! Merci à lui\r\n!\"', '', 17, 4),
(72, 'Dupilon', 'Spirou/Dupilon.jpg', 'Personnage secondaire de Spirou et Fantasio, Célestin Dupilon habite Champignac et est continuellement ivre.', '', 170, 4);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `last_name`, `first_name`, `email`, `password`, `is_admin`) VALUES
(1, 'aaa', 'aaa', 'amanda.elfassy@outlook.fr', '4124bc0a9335c27f086f24ba207a4912', 0),
(2, 'ELFASSY', 'AMANDA', 'a.giami@wanadoo.fr', '74b87337454200d4d33f80c4663dc5e5', NULL),
(3, 'D\'amelio', 'charli', 'admin@thebrickbox.net', '2a4b581726ea718d4348d2591b87b8d7', NULL),
(4, 'S', 'S', 'S', '5dbc98dcc983a70728bd082d1a47546e', NULL),
(5, 'E', 'E', 'E', '3a3ea00cfc35332cedf6e5e9a32e94da', NULL),
(6, 'ELFASSY', 'AMANDA', 'cha@outloo', '74b87337454200d4d33f80c4663dc5e5', NULL),
(7, 'jemesensmalsicamarche', 'essaie', 'hashemaidemoi', '6050ce63e4bce6764cb34cac51fb44d1', NULL),
(8, 'zz', 'zz', 'zz', '25ed1bcb423b0b7200f485fc5ff71c8e', NULL),
(9, 'ee', 'ee', 'ee', 'c5ecd67127d58675ec80ceeb427924df', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `productsimage_id` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `panier_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `category_link` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
